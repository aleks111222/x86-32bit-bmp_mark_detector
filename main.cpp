
#include <stdio.h>
#include <stdlib.h>

extern "C" int find_markers(unsigned char* bitmap,
			unsigned int* x_pos,
			unsigned int* y_pos);


unsigned int x_pos[5];
unsigned int y_pos[5];

int main() {

  FILE* f = fopen("test7.bmp", "rb");

  if (f == NULL) {
        perror("The program encountered an error while opening the file");
        return -1;
  }
  puts("The file was successfully opened");

  fseek(f, 0, SEEK_END);
  int fsize = ftell(f);
  fseek(f, 0, SEEK_SET);

  unsigned char* bitmap = (unsigned char*) malloc(fsize + 1);

  fread(bitmap, 1, fsize, f);
  fclose(f);

  bitmap[fsize] = 0;

//  printf("Size of the file: %d\n", fsize);

  find_markers(bitmap, x_pos, y_pos);

  for(int i = 0; i < 5; i++) {

	if(i > 0 && x_pos[i] == 0 && y_pos[i] == 0) continue;

  	printf("%d %d \n", x_pos[i], y_pos[i]);
  }

  return 0;
}
