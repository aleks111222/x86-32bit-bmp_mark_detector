;=====================================================================
; ECOAR - BMP MARKER DETECTOR
;
; Author:      Rębisz Aleksander
; Date:        2021
; Description:
;              int find_markers(unsigned char* bitmap, unsigned int* x_pos, unsigned int* y_pos);
;
;=====================================================================
section .data
	bitmap dw 0
	reg1 dw 0

section	.text
global  find_markers

find_markers:

	push	ebp
	mov	ebp, esp
	push	ebx
	push	edi

	;mov	eax, DWORD [ebp + 12]
	;mov	ebx, 1
	;imul	ebx, 4
	;mov	DWORD [eax + ebx], edi

	;pop	edi
	;pop	ebx
	;pop	ebp
	;ret

	mov	edi, 0

	mov	eax, DWORD [ebp + 8] ; bitmap
	mov	[bitmap], eax ; store bitmap globally
	mov	ebx, [eax + 18] ; storing width
	push	ebx
	mov	ebx, [eax + 22] ; storing height
	push 	ebx

	xor	eax, eax ; beginning coordinates
	push	eax ; y
	push	eax ; x

main_loop:

	mov	ecx, [ebp - 24] ;  x
	mov	ecx, [ebp - 20]
	mov	ecx, [ebp - 24]

	mov	eax, [ebp - 12]
	cmp	ecx, eax; if width is max
	je	new_line

	call 	get_pixel

	lea	ebx, [ebp - 24]
	add	DWORD [ebx], 1

	cmp 	eax, 0x00000000
	jne 	main_loop

	mov	eax, 0
	push	eax
	push	eax
	push	eax
	push	eax
	push	eax
	mov	eax, ecx
	push 	eax
	mov	eax, [ebp - 20]
	push	eax
	push 	eax
	mov	eax, ecx
	push	eax

check_valid_x:

	mov	eax, [ebp - 56]
	sub	eax, 1

	mov	DWORD [ebp - 44], 1
	cmp	eax, 0
	jl	check_length_x

	sub	DWORD [ebp - 56], 1
	call	get_pixel
	add	DWORD [ebp - 56], 1

	cmp	eax, 0x00000000
	je	loop_exit

check_length_x:

	mov	eax, [ebp - 28]
	mov	eax, [ebp - 60]
	mov	ebx, [ebp - 12]
	;sub	ebx, 1
	cmp	eax, ebx
	je	check_length_y

	call	get_pixel
	mov	ebx, [ebp - 44]
	cmp	ebx, 1
	jne	check_valid_x
	add	DWORD [ebp - 60], 1
	cmp	eax, 0x00000000
	jne	check_length_y
	add	DWORD [ebp - 28], 1
	mov	DWORD [ebp - 44], 0

	jmp	check_length_x

check_length_x_further:

	mov	eax, [ebp - 60]
	mov	ebx, [ebp - 12]
	;sub	ebx, 1
	cmp	eax, ebx
	je	check_length_y_further

	sub	eax, [ebp - 48]
	mov	ebx, eax
	mov	eax, [ebp - 36]
	add	eax, 1
	cmp	ebx, eax
	je	check_length_y_further

	call	get_pixel
	add	DWORD [ebp - 60], 1
	cmp	eax, 0x00000000
	jne	check_length_x_further
	add	DWORD [ebp - 28], 1

	jmp	check_length_x_further

check_valid_y:

	mov	eax, [ebp - 60]
	sub	eax, 1

	mov	DWORD [ebp - 40], 1
	cmp	eax, 0
	jl	check_length_y

	sub	DWORD [ebp - 60], 1
	call	get_pixel
	add	DWORD [ebp - 60], 1

	cmp	eax, 0x00000000
	je	loop_exit

check_length_y:

	mov	eax, [ebp - 32]
	mov	eax, [ebp - 48]
	mov	[ebp - 60], eax
	mov	eax, [ebp - 56]
	mov	ebx, [ebp - 16]
	;sub	ebx, 1
	cmp	eax, ebx
	je	compare_1

	call	get_pixel
	mov	ebx, [ebp - 40]
	cmp	ebx, 1
	jne	check_valid_y
	add	DWORD [ebp - 56], 1
	cmp	eax, 0x00000000
	jne	compare_1
	add	DWORD [ebp - 32], 1
	mov	DWORD [ebp - 40], 0

	jmp	check_length_y

check_length_y_further:

	mov	eax, [ebp - 48]
	mov	[ebp - 60], eax

	mov	eax, [ebp - 56]
	sub	eax, [ebp - 52]
	mov	ebx, eax

	mov	eax, [ebp - 36]
	add	eax, 1

	cmp	ebx, eax
	je	compare_2
	mov	eax, [ebp - 56]
	mov	ebx, [ebp - 16]
	;sub	ebx, 1
	cmp	eax, ebx
	je	compare_2

	call	get_pixel
	add	DWORD [ebp - 56], 1
	cmp	eax, 0x00000000
	jne	check_length_y_further
	add	DWORD [ebp - 32], 1

	jmp	check_length_y_further

compare_2:

	mov	eax, [ebp - 28]
	mov	eax, [ebp - 32]

	mov	eax, [ebp - 28]
	cmp	eax, [ebp - 32]
	jg	loop_exit

	cmp	[ebp - 32], eax
	jg	loop_exit

	cmp	eax, 0
	je	store_coords

	cmp	eax, [ebp - 36]
	jne	loop_exit

	cmp	eax, 1
	je	loop_exit

	jmp	check_layers

compare_1:

	mov	eax, [ebp - 28]
	mov	eax, [ebp - 32]

	mov	eax, [ebp - 28]
	cmp	eax, 0x00000000
	je	loop_exit

	cmp	eax, [ebp - 32]
	jne	loop_exit

check_layers:

	mov	eax, [ebp - 32]
	sub	eax, 1
	mov	[ebp - 36], eax

	add	DWORD [ebp - 48], 1
	add	DWORD [ebp - 52], 1

	mov	eax, [ebp - 48]
	mov	[ebp - 60], eax
	mov	eax, [ebp - 52]
	mov	[ebp - 56], eax

	mov	DWORD [ebp - 28], 0
	mov	DWORD [ebp - 32], 0

	mov	eax, [ebp - 36]
	cmp	eax, 0x00000000
	je	loop_exit
	jmp	check_length_x_further

store_coords:

	mov	eax, [ebp - 24]
	sub	eax, 1
	mov	ecx, DWORD [ebp + 12]
	mov	edx, edi
	imul	edx, 4
	mov	DWORD [ecx + edx], eax

	mov	eax, [ebp - 16]
	sub	eax, 1
	sub	eax, [ebp - 20]
	mov	ecx, DWORD [ebp + 16]
	mov	DWORD [ecx + edx], eax

	inc	edi

loop_exit:

	add	esp, 36
	jmp	main_loop

exit:

	add	esp, 16
	pop	edi
	pop	ebx
	pop	ebp
	ret

new_line:

	add	DWORD [ebp - 20], 1
	mov	eax, [ebp - 20]
	cmp	eax, [ebp - 16] ; if height is max
	je	exit
	mov	DWORD [ebp - 24], 0
	jmp	main_loop

get_pixel: ; arguments: , returns: eax = "00RRGGBB", modifies: eax, ecx, ebx, edx

	push 	ebp
	mov	ebp, esp
	push	ecx
	push 	ebx

	mov	eax, [bitmap]
	mov	ebx, [eax + 18]
	mov	ecx, [ebp + 12]

	imul	ebx, 3
	add	ebx, 3
	and	ebx, 0xFFFFFFFC
	imul	ebx, ecx

	mov	edx, [ebp + 8]
	imul	edx, 3
	add	ebx, edx
	add	ebx, eax
	add	ebx, 54

	mov	eax, [ebx]
	and	eax, 0x00FFFFFF

	pop	ebx
	pop	ecx
	pop	ebp
	ret


;============================================
; THE STACK
;============================================
;
; larger addresses
;
;  |                               |
;  | ...                           |
;  ---------------------------------
;  | function parameter - char *a  | EBP+8
;  ---------------------------------
;  | return address                | EBP+4
;  ---------------------------------
;  | saved ebp                     | EBP, ESP
;  ---------------------------------
;  | ... here local variables      | EBP-x
;  |     when needed               |
;
; \/                              \/
; \/ the stack grows in this      \/
; \/ direction                    \/
;
; lower addresses
;
;
;============================================
